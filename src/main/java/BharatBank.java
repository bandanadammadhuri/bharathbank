public class BharatBank {
    private int accountBalance;
    final String typeOfAccount;
    final int interestForCurrentAccountInPercentage = 3;
    final int interestForSavingsAccountInPercentage = 0;

    public BharatBank(String account, int balance) {
        typeOfAccount = account;
        accountBalance = balance;
    }

    public String withDrawingMoney(int withdrawAmount) {
        if (this.accountBalance < withdrawAmount) {
            return "Insufficient amount";
        }
        this.accountBalance = accountBalance - withdrawAmount;
        return "Withdraw by Amritha is :" + withdrawAmount;
    }

    public String depositMoney(int depositedAmount) {
        this.accountBalance = accountBalance + depositedAmount;
        return "Amount deposited by Gopal is :" + depositedAmount;
    }

    public double calculateInterest() {
        int rateOfInterest;
        final double interestRate = 0.24;
        if (this.typeOfAccount == "Current Account") {
            rateOfInterest = interestForCurrentAccountInPercentage;
        } else {
            rateOfInterest = interestForSavingsAccountInPercentage;
        }
        double balanceAlongWithInterest = rateOfInterest * this.accountBalance;
        return (double) balanceAlongWithInterest * interestRate / 100;
    }

    public static void main(String[] args) {
        BharatBank gopalAccount = new BharatBank("current", 50000);
        System.out.println(gopalAccount.depositMoney(10000));
        BharatBank amrithaAccount = new BharatBank("savings", 100000);
        System.out.println(amrithaAccount.withDrawingMoney(45000));
        System.out.println("Interest of Gopal is : " + gopalAccount.calculateInterest());
        System.out.println("Interest of Amritha is : " + amrithaAccount.calculateInterest());
    }
}
